--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.4
-- Dumped by pg_dump version 9.6.4

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: alembic_version; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE alembic_version (
    version_num character varying(32) NOT NULL
);


ALTER TABLE alembic_version OWNER TO postgres;

--
-- Name: business_model; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE business_model (
    id integer NOT NULL,
    name character varying(50),
    description character varying(150),
    code character varying(50)
);


ALTER TABLE business_model OWNER TO postgres;

--
-- Name: business_model_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE business_model_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE business_model_id_seq OWNER TO postgres;

--
-- Name: business_model_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE business_model_id_seq OWNED BY business_model.id;


--
-- Name: capability; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE capability (
    id integer NOT NULL,
    name character varying(200) NOT NULL,
    code character varying(50)
);


ALTER TABLE capability OWNER TO postgres;

--
-- Name: capability_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE capability_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE capability_id_seq OWNER TO postgres;

--
-- Name: capability_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE capability_id_seq OWNED BY capability.id;


--
-- Name: category; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE category (
    id integer NOT NULL,
    name character varying(200) NOT NULL,
    code character varying(50)
);


ALTER TABLE category OWNER TO postgres;

--
-- Name: category_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE category_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE category_id_seq OWNER TO postgres;

--
-- Name: category_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE category_id_seq OWNED BY category.id;


--
-- Name: company; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE company (
    id integer NOT NULL,
    name character varying(200),
    description character varying(1500),
    phone text NOT NULL,
    email text NOT NULL,
    website text NOT NULL,
    twitter text NOT NULL,
    linkedin text NOT NULL,
    logo character varying(250),
    owner_id integer,
    key_features character varying(1500) NOT NULL
);


ALTER TABLE company OWNER TO postgres;

--
-- Name: company_businessmodel; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE company_businessmodel (
    business_model_id integer,
    company_id integer
);


ALTER TABLE company_businessmodel OWNER TO postgres;

--
-- Name: company_capabilities; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE company_capabilities (
    capability_id integer,
    company_id integer
);


ALTER TABLE company_capabilities OWNER TO postgres;

--
-- Name: company_category; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE company_category (
    category_id integer,
    company_id integer
);


ALTER TABLE company_category OWNER TO postgres;

--
-- Name: company_group; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE company_group (
    group_id integer,
    company_id integer
);


ALTER TABLE company_group OWNER TO postgres;

--
-- Name: company_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE company_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE company_id_seq OWNER TO postgres;

--
-- Name: company_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE company_id_seq OWNED BY company.id;


--
-- Name: company_location; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE company_location (
    location_id integer,
    company_id integer
);


ALTER TABLE company_location OWNER TO postgres;

--
-- Name: company_partner; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE company_partner (
    partner_id integer,
    company_id integer
);


ALTER TABLE company_partner OWNER TO postgres;

--
-- Name: groupco; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE groupco (
    id integer NOT NULL,
    name character varying(200) NOT NULL,
    code character varying(50)
);


ALTER TABLE groupco OWNER TO postgres;

--
-- Name: groupco_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE groupco_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE groupco_id_seq OWNER TO postgres;

--
-- Name: groupco_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE groupco_id_seq OWNED BY groupco.id;


--
-- Name: location; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE location (
    id integer NOT NULL,
    name character varying(200) NOT NULL,
    geo_location character varying(200) NOT NULL
);


ALTER TABLE location OWNER TO postgres;

--
-- Name: location_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE location_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE location_id_seq OWNER TO postgres;

--
-- Name: location_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE location_id_seq OWNED BY location.id;


--
-- Name: partner; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE partner (
    id integer NOT NULL,
    name character varying(200) NOT NULL
);


ALTER TABLE partner OWNER TO postgres;

--
-- Name: partner_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE partner_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE partner_id_seq OWNER TO postgres;

--
-- Name: partner_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE partner_id_seq OWNED BY partner.id;


--
-- Name: user; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE "user" (
    id integer NOT NULL,
    username character varying(80),
    email character varying(120),
    password_hash character varying
);


ALTER TABLE "user" OWNER TO postgres;

--
-- Name: user_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE user_id_seq OWNER TO postgres;

--
-- Name: user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE user_id_seq OWNED BY "user".id;


--
-- Name: business_model id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY business_model ALTER COLUMN id SET DEFAULT nextval('business_model_id_seq'::regclass);


--
-- Name: capability id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY capability ALTER COLUMN id SET DEFAULT nextval('capability_id_seq'::regclass);


--
-- Name: category id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY category ALTER COLUMN id SET DEFAULT nextval('category_id_seq'::regclass);


--
-- Name: company id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY company ALTER COLUMN id SET DEFAULT nextval('company_id_seq'::regclass);


--
-- Name: groupco id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY groupco ALTER COLUMN id SET DEFAULT nextval('groupco_id_seq'::regclass);


--
-- Name: location id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY location ALTER COLUMN id SET DEFAULT nextval('location_id_seq'::regclass);


--
-- Name: partner id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY partner ALTER COLUMN id SET DEFAULT nextval('partner_id_seq'::regclass);


--
-- Name: user id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "user" ALTER COLUMN id SET DEFAULT nextval('user_id_seq'::regclass);


--
-- Data for Name: alembic_version; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY alembic_version (version_num) FROM stdin;
5caa93ed35a6
\.


--
-- Data for Name: business_model; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY business_model (id, name, description, code) FROM stdin;
1	B2B	Business-to-Business	B2B
2	B2C	Business-to-Consumer	B2C
\.


--
-- Name: business_model_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('business_model_id_seq', 1, false);


--
-- Data for Name: capability; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY capability (id, name, code) FROM stdin;
1	In house	IN_HOUS
2	Not technically inclined. Have online application process.	NTI_OPP
3	Partners with	PWT
\.


--
-- Name: capability_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('capability_id_seq', 1, false);


--
-- Data for Name: category; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY category (id, name, code) FROM stdin;
1	Lending	LEND
2	Banking and Personal Financial Management(PFM)	BPFM
3	Payments and Money transfer	PAY_MON
4	Cyber Currency & Blockchain	CYB_CUR_BLK
5	Wealth & Investment Management	WLH_INV_MNG
6	Robo Advisor	ROB_ADV
7	Financial Crime & Cybersecurity	FIN_CRM_CYB
8	Capital Markets	CAP_MRK
9	Artificial Intelligence	AI
10	Accounting	ACCN
\.


--
-- Name: category_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('category_id_seq', 1, false);


--
-- Data for Name: company; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY company (id, name, description, phone, email, website, twitter, linkedin, logo, owner_id, key_features) FROM stdin;
1	Amber	Amber Financial offers financing for your home renovation, debt consolidation, vacation, wedding, personal purchase in addition to any other financial need. 	+1 (866) 268-0328	info@amberfinancial.com	https://amberfinancial.com/en	https://twitter.com/amber_financial	https://www.linkedin.com/company-beta/7577942/	\N	\N	Mortgage
2	Aspire Financial Technologies	The Aspire Gateway platform is a software infrastructure solution enabling better loan data, reporting and analytics flow between Loan Originators, Investors and Banks.			http://aspirefintech.com/	https://twitter.com/AspireFinTech1	https://www.linkedin.com/company-beta/10538817/	\N	\N	Loan analytics, Alternative Lending, Marketplace Lending
3	Borrowell	We’re a safe responsible new way for Canadians to obtain three and five year fixed-rate loans at low interest rates that reward your good credit score. And since your credit score is so important to managing your financial life, we’re also a secure way to get your credit score—for free.	1-888-285-0990	hello@borrowell.com	https://www.borrowell.com/	https://twitter.com/Borrowell	https://www.linkedin.com/company-beta/4036218/	\N	\N	Includes P2P lending
4	Canada Drives	With Canada Drives You Have Access to Our Nation Wide Network of Dealer Partners and Many Types of Vehicles	1-888-865-6402			https://twitter.com/companycapital	https://www.linkedin.com/company-beta/2702862/	\N	\N	Car Loans
5	Company Capital	Company Capital is what is referred to as a direct lender – meaning you are dealing directly with the decision makers and source of the money. We provide online loans to small businesses in Canada	1-877-595-2346	info@companycapital.ca	https://www.companycapital.ca/canadian-small-business-loans/	https://twitter.com/companycapital	https://www.linkedin.com/company-beta/2702862/	\N	\N	
6	Crelogix	Crelogix Credit Group Inc. is the parent company utilized by a group of finance and credit services industry entrepreneurs to incubate and develop financing, credit, insurance and leasing products and platforms. 	(800) 667-6640	support@crelogix.com	http://www.crelogix.com 		https://www.linkedin.com/company-beta/784085/	\N	\N	
7	Do Your Own Mortgage	Do Your Own Mortgage is a self-service mortgage website that allows you to take control of your mortgage application and receive a Cash Rebate on all mortgage products. By doing your own mortgage, you will be able to track the progress of your application every step of the way through your private dashboard.	647-931-3966	questions@doyourownmortgage.com	https://www.doyourownmortgage.com/	https://twitter.com/DYOMC	https://www.linkedin.com/company-beta/10151487/	\N	\N	
8	Financeit	Financeit is a market leading point-of-sale financing provider servicing the home improvement, vehicle and retail industries. Financeit’s innovative cloud-based technology makes it easy for merchants to increase close rates and transaction sizes with affordable monthly or bi-weekly payment plans.	1-888-536-3025	%20service@financeit.io	https://www.financeit.io/ca/en/program	https://twitter.com/financeit	https://www.linkedin.com/company-beta/824742/	\N	\N	
9	Finstripe	Finstripe connects professionals to the enterprise-level corporate lending world from their desks and mobile devices. They can post unlimited borrowing requests, search the globe for compatible lenders or borrowers, monitor specific industry or geographic opportunities, and connect with high probability partners around the clock from the convenience of your internet connected desktop, phone or tablet.		info@finstripe.com	http://www.finstripe.com/	https://twitter.com/finstripe		\N	\N	
10	info@poweredbygrow.com	Flexiti Financial is a Canadian sales financing company founded in 2013. We help retailers increase their bottom line by providing better sales financing options than a standard credit card, with lower interest rates, longer payment terms and revolving credit. Using a mobile application process and patented ID scanning technology, we provide you with quick, simple and instant Point-of-Sale (POS) financing for your customers. 	 1-855-559-0909		http://flexitifinancial.com/i-am-a-business/			\N	\N	
11	Fundthrough	We are a lender specialized in lines of credit backed by outstanding invoices. We help business that traditional banks tend to overlook, access the money they need to continue growing. Our operating loans are based on the profile of our client`s customers, instead of the strength of their credit history or their balance sheet. 	(416) 971-5724	info@fundthrough.com	https://www.fundthrough.com/	https://twitter.com/fundthrough		\N	\N	
12	GoDay	Online payday loan company, GoDay.ca, provides highly automated short term loans that require no paperwork or faxing, and can be completed at any desktop computer, tablet, or mobile device. With transparent policies, accessibility, and responsible lending as their foundation, applicants can be sure that a loan from GoDay.ca is fast, smart, and secure. 	1-888-984-6329		https://goday.ca/	https://twitter.com/godayca		\N	\N	
13	Grow	We leverage the power of technology to bring together creditworthy borrowers seeking loans with investors looking to earn a fair return on their money — all in a convenient online environment that guarantees timely, personalized service with very competitive interest rates.	1 888 540 3951	info@poweredbygrow.com	https://www.poweredbygrow.com/	https://twitter.com/poweredbygrow	https://www.linkedin.com/company-beta/807133/	\N	\N	
14	Impak Finance	Impak Finance introduces the Impak Coin : the first cryptocurrency coded to grow the impact economy. First, we are going to launch an impact investment fund online. Our 100% digital and mobile platform will enable anyone to invest in impact bonds.		hello@impakfinance.com	https://mpk.impak.eco/en/	https://twitter.com/impakfinance	https://www.linkedin.com/company-beta/10832737/	\N	\N	
15	Intellimortgage	Online mortgage broker and lender using proprietary mortgage rate comparison technology and a deep discount "Do-it-Yourself" model to offer Canadians the best possible mortgage rates.	 1-800-280-2460	info@intellimortgage.com	https://intellimortgage.com/	https://twitter.com/intelliMortgage		\N	\N	Mortgage
16	IOU Financial	IOU Financial, Inc., through a subsidiary, operates an Internet lending platform. The Company serves small businesses that are often under served by banks.	1.866.217.8564		https://ioufinancial.com/	https://twitter.com/ioufinancial	https://www.linkedin.com/company-beta/5121483/	\N	\N	
17	Kanetix	Kanetix Ltd. operates Kanetix.ca, an online insurance and mortgage marketplace that provides online comparison shopping services in Canada. It offers real-time insurance and mortgage quotes. The company enables users to compare the insurers and their rates, payment options, and available discounts. It also provides consulting and development services, which include Web development consulting, online quotation systems, XML data transfer and Web, online marketing, ‘White labeled’ or private labeled Websites, and Website hosting.	416-598-5401	support@kanetix.ca 	https://www.kanetixltd.ca/	https://twitter.com/kanetix		\N	\N	Insurance
18	Kosmo Credit	Kosmo is a new company that specialized in giving people fast and reliable loans. Kosmo utilizes the highly secure information gathering technology to gather user information and give clients their customized rates and loan amount based on the information they gave us. We make it simple and fast for people to get loans, and we make direct deposits to clients` bank account as fast as next day. Kosmo Credit is currently operating in all provinces within Canada.		 info@waveapps.com.	https://www.waveapps.com/		https://www.linkedin.com/company-beta/9424191/	\N	\N	Online Finance, Consumer Finance, Personal Loans, and Innovative Technology
19	Lendful	Personal Loans based on credit raitng and application approvals. 		info@lendful.ca	https://lendful.ca/#/home	https://twitter.com/lendfulloans	https://www.linkedin.com/company-beta/6425659/	\N	\N	Consumer lending, Marketplace Lending, Peer-to-Peer Lending, and P2P
20	Lending Loop	Connecting small businesses with Canadian investors is Lending Loop`s way to help these small businesses grow. Lending Loop is Canada`s first fully regulated peer-to-peer lending platform focused on small business.	(888) 223-5667	contact@lendingloop.ca	https://www.lendingloop.ca/about	https://twitter.com/lendingloop	https://www.linkedin.com/company-beta/5166811/	\N	\N	
21	Lendified	Lendified is Canada`s premier online lender for small businesses and offers a customizable platform available to financial institutions throughout the world.	1.844.451.3594		https://www.lendified.com/	https://twitter.com/lendified	https://www.linkedin.com/company-beta/10043433/	\N	\N	
22	Lendingarch	LendingArch is a modern finance company reinventing the lending experience. Through smart technology and exceptional customer experience, LendingArch has become one of Canada’s fastest-growing providers of instant credit to financially responsible Canadians. From medical expenses, debt consolidation, home improvement costs and help with personal growth, LendingArch is dedicated to bringing financial advancement to consumers who deserve to live a happy life, at their rate.	+1-844-900-Arch	 hello@lendingarch.com	https://lendingarch.ca/		https://www.linkedin.com/company-beta/10017410/	\N	\N	
23	Marble Financial	Marble Financial (MLI Marble Lending, Inc) is developing Canada`s first mobile/online Marketplace Lending Platform (MLP) offering clients a convenient, personalized, full-circle solution for managing debt, tracking budgets and monitoring personal financial health. Marble’s proprietary, scalable, cloud-based infrastructure and dashboard provide a diverse portfolio of loan products and services.	604–336–0185	info@marblefinancial.ca	https://marblefinancial.ca/	https://twitter.com/marble2go	https://www.linkedin.com/company-beta/10219196/	\N	\N	Fintech, Online Security, Software Development, API Development, Integration Services, Project Management, VPNs, Firewalls, and Financial Dashboard
24	Merchant Advance Capital	Our innovative approach blends thoughtful customer care, complete transparency and smart technology to provide accessible financing that makes sense. Merchant Advance Capital`s modern financing option allows small business owners fast & easy, no-collateral access to working capital, with flexible payback options responsive to the ebbs and flows of business cycles.	604-757-4602	clients@merchantadvance.com	https://www.merchantadvance.com/	https://twitter.com/advancecapital	https://www.linkedin.com/company-beta/1197889/	\N	\N	Alternative Lending, Lending, Small Business Lending, Business Loans, Line of Credit, and Equipment Leasing
25	Mogo	By leveraging technology and design we`re building a financial brand that`s transforming the way Canadians access, manage, and control credit. 			https://www.mogo.ca/	https://twitter.com/mogomoney/		\N	\N	Personal  Loans, Mortgage, Credit Score, Spending account
26	OnDeck Capital	Created to provide capital to small businesses. Evaluates business’ actual performance, not just personal credit.	 (888) 727-6183		https://en-ca.ondeck.com/company/	https://twitter.com/OnDeckCapital/		\N	\N	
27	Progressa	Progressa is Canada`s only `Direct-Pay`​ lender focused solely on non-prime and near-prime consumers. Our online technology platform "Powered by Progressa"​ allows us to provide our B2B partners a seamless borrowing experience for their customers, and our data-driven underwriting approach allows us to work through complex financial situations in 15 minutes or less.	1-855-723-5626	info@progressa.com	http://progressa.com/	https://twitter.com/progressacanada	https://www.linkedin.com/company-beta/9479920/	\N	\N	
28	Ratehub	RateHub is an independent website dedicated to connecting you to the best mortgage rates and credit card deals in Canada.	(416) 901-6187	admin@ratehub.ca	https://www.ratehub.ca/	https://twitter.com/RateHub	https://www.linkedin.com/company-beta/2349594/	\N	\N	Mortgage rates, Mortgage calculators, Mortgage rate API, Financial Services leads, Credit card deals, and Credit card comparisons
29	Thinking Capital	Thinking Capital is the leader in the Canadian Alternative Lending space, leveraging technology to be at the forefront of the FinTech industry. Since 2006, they have helped more than 10,000 small-to-medium sized Canadian businesses reach their full potential. By combining their people and technology, Thinking Capital is transforming the way business owners get the capital they need to grow.	866-889-9412	info@thinkingcapital.ca	https://www.thinkingcapital.ca/	https://twitter.com/ThinkingCapCA	https://www.linkedin.com/company-beta/1402251/	\N	\N	
30	Vault Circle	Vault Circle Inc. is a digital lending investment firm that will connect accredited investors with short-term, high-yield small business loans in the form of investor notes. These investments will appeal to investors seeking income and strong portfolio cash flows. Vault Circle is an affiliate of Lendified Inc., a premier online provider of working capital loans to Canadian small businesses	 416.861.8181ext. 510	marcel.schroder@vaultcircle.com	http://www.vaultcircle.com/	https://twitter.com/vaultcircleinc	https://www.linkedin.com/company-beta/9345644/	\N	\N	Investing, and Marketplace Lending
31	Wonga	Wonga Group Limited is made up of a number of international financial services businesses. We are best known for our Wonga short term loan product.	0207 138 8330	customercare@wonga.com	https://www.wonga.com/			\N	\N	We provide automated decisions to applicants based on the information they provide in the application, data from Credit Reference Agencies and other sources. All applicants are credit checked and need to pass stringent lending and affordability criteria.
32	Zayzoon	With ZayZoon, employees and staff can access their funds in advance of their pay day. Provided at no cost to employers - this is the secure, easy and private way to provide advances to employees.Employees access ZayZoon from their mobile device or through the PayChequer site			zayzoon.com	https://twitter.com/zayzoontransfer	https://www.linkedin.com/company-beta/4825335/	\N	\N	Integrated with PayChequer
58	ChangeJAr	ChangeJar is a mobile app that allows you to carry cash on your phone. Use that cash to: TIP, TRANSFER, PAY. Payments with ChangeJar are quick and easy. All it takes is a scan of your phone and it’ll be processed in seconds.		info@changejar.com	https://www.changejar.com/	https://twitter.com/ChangeJarMobile		\N	\N	
33	PitchPoint	PitchPoint Solutions is a national provider of comprehensive fraud detection and verification services for multiple industries, including mortgage, background/ tenant screening, and anti-money laundering. With more than a decade of experience in customizable risk evaluation technology and processing, PitchPoint Solutions` heuristic/rule-based data collation, analysis and Expert Services stamps out 99% of fraud alerts while eliminating 100% of lost underwriting time.	416.591.7476		http://www.pitchpointsolutions.com/		https://ca.linkedin.com/company/pitchpoint-solutions	\N	\N	Fraud Detection, Mortgage Data Validation, Fraud Compliance Review, Mortgage Loan Portfolio Review
34	Zensurance	Zensurance is leading the technology revolution in the insurance industry, and demystifying insurance for start-ups and small businesses. By using data and analytics to identify the most common risks, Zensurance is able to create curated insurance packages ideally suited for each industry. Small business owners no longer have to wade through endless forms and play phone-tag: they can learn about all their needs and purchase at their own convenience within minutes online. 			https://www.zensurance.com/about	https://twitter.com/zensurance	https://www.linkedin.com/company-beta/10486499/	\N	\N	
35	Fentury	Efficient finance management is about saving both money and time. With Fentury you can connect all your bank and e-Wallet accounts and we’ll automatically structure your data and always keep you up-to-date. FENTURY is a next generation automatic personal finance manager. You can automatically import data from your financial accounts or manage your cash flow from one place to better understand your financial situation. Keep track on your finances with multiple reports and plan for the future with Fentury budgets. Get your finances under control!		hello@fentury.com	https://www.fentury.com	https://twitter.com/fenturyapp	https://www.linkedin.com/company-beta/10003220/	\N	\N	Personal Finance Management, Automatic Finance Management, Financial WEB Manager, and Financial Mobile App
36	Finn.ai	Finn.ai is a full-featured conversational banking platform. Our white-label chatbots are personal financial management assistants, powered by AI. Banks and other financial institutions are able to put a personal banker and financial advisor in every customer`s pocket; reach them on any chat platform, native app, website chat interface and more. 		sales@finn.ai	http://finn.ai/	https://twitter.com/finnforbanks	https://www.linkedin.com/company-beta/10062817/	\N	\N	
37	Koho	Our Smart Spending Account lets you send transfers, pay bills, save automatically, use ATMs, while letting you know how you spend – everything you need for the modern world and it’s all free. 	1-855-564-6999	 team@koho.ca	https://www.koho.ca/		https://twitter.com/GetKoho	\N	\N	Koho offers the Koho Visa* Prepaid Card and mobile app that together allow users to manage daily financial needs (direct paycheque load, bill pay, atm cash withdrawals, etc.), while offering tools such as automated savings goals, real-time updates, transfers, spending insights and categorizations and much more.
38	perfiqt	AI to help you answer your toughest financial questions. Fast. Easy. Free And we can do it for millions of people. Personally Perfiqt replaces traditional financial planning with fun and engaging question-answer process In just a few minutes you can visualize your entire life and get personal insights to help you make smarter life decisions yourself. 				https://twitter.com/perfiqt	https://www.linkedin.com/company-beta/10817939/	\N	\N	
39	BitGold	On May 22, 2015, BitGold Inc. announced the acquisition of GoldMoney Inc. The Goldmoney™ App for iOS and Android provides access to all Dashboard features and functions, enabling you to manage your Holding and buy, sell, exchange, and redeem precious metal bullion through an easy-to-use and highly secure interface. Need to make a payment? Send and accept precious metal payments to and from other Goldmoney Holdings and businesses, and earn metals by referring friends and family to Goldmoney.	 1-855-583-(4653)		https://www.goldmoney.com/	https://twitter.com/Goldmoney	https://www.linkedin.com/company/5357470/	\N	\N	Powered by Goldmoney’s patented technology, the Goldmoney Holding® is an online account that enables clients to invest, earn, or spend gold, silver, platinum, and palladium bullion that is securely stored in insured vaults located around the world.
40	EQBank	At EQ Bank, our digital banking division, we’re focused on building a better banking experience. With free everyday banking that you can access from your phone, EQ Bank makes sense in today’s world. 	1-844-437-2265	contact@eqbank.ca	https://www.eqbank.ca	https://twitter.com/EQBank	https://www.linkedin.com/company-beta/515301/	\N	\N	Not technically inclined. Have online application process.
41	Mint	Mint.com is a free, web-based personal financial management service for the US and Canada, created by Aaron Patzer. Mint originally provided account aggregation through a deal with Yodlee, but has since moved to using Intuit for connecting to accounts. Mint`s primary service allows users to track bank, credit card, investment, and loan balances and transactions through a single user interface, as well as create budgets and set financial goals. In 2009, Mint was acquired by Intuit, the makers of Quicken and TurboTax.			https://www.mint.com/	https://twitter.com/mint		\N	\N	Acquired by Intuit
42	OneLife Financial Solutions	ONELIFE Wealth Management is your solution-based financial planning organization. We know that what you keep is far more important than what you make. Work with us to build a customized plan to secure your financial future.	613-741-5460	info@onelifewealth.com	http://www.onelifewealth.com/		https://www.linkedin.com/company-beta/2019887/	\N	\N	Debt Elimination Solutions
43	Proliteracy	Proliteracy helps in seeing the complete financial picture. Forecast cost of an education. Explore financing options and Learn from the community. Proliteracy.ca uses open data to help Canadians plan financially for post-secondary education.			hello@proliteracy.ca	https://twitter.com/proliteracyca		\N	\N	Proliteracy.ca for business, Proliteracy.ca for schools
44	RateSeer	RateSeer is a new fintech startup that helps consumers find the most competitive banking, mortgage, credit card and loan rates. RateSeer also offers services to financial professionals that provide them with up to the minute detection and notification of changes to thousands of market rates, commodities, currency and economic indicators.		info@rateseer.com	http://www.rateseer.com/	https://twitter.com/rateseerco		\N	\N	RateSeer is always monitoring thousands of banks, credit card companies and loan providers to look for changes in service rates, terms and offerings.
59	Control	Control is a leader in mobile payment analytics and alerts for SaaS, subscription and eCommerce businesses, enabling instant intelligence anywhere via its Android, iOS, and web-based products. Control consolidates data from multiple sources such as PayPal, Square and Stripe, to provide real-time revenue and customer information, fraud detection, and key metrics customized to your business type — instantly.			https://www.getcontrol.co/	https://twitter.com/GetControlApp	https://www.linkedin.com/company-beta/3660959/	\N	\N	Stripe, Mobile App, Payments, PayPal, Square, Analytics, Web App, and Omnichannel Analytics
45	Sensibill	Sensibill allows banking customers to manage their line-item receipts directly from their desktop and mobile banking applications. Customers use the service to keep track of important receipts for returns, exchanges, expenses, accounting, and taxes. Sensibill also leverages item-level purchase data to deliver additional value to customers including enhanced personal financial management and reminders for return and warranty expiry dates. We help power the future of banking innovation to allow financial institutions to better engage and retain their customers, leading to increased revenue and cost savings. 			http://getsensibill.com	https://twitter.com/getsensibill	https://www.linkedin.com/company-beta/3513561/	\N	\N	Digital Receipts, Omni-channel, Digital Banking, and Personal Financial Management
46	Zag Bank	Zag Bank is a part of the Desjardins Group, the leading cooperative financial group in Canada and the sixth largest cooperative financial group in the world. That has always provided us with the stability and resources we need to offer Canadians a new way to bank today and in the future. We started serving clients as Bank West in 2003 and changed our name to Zag Bank in 2014 to reflect our commitment to doing things differently.  we have chosen to offer all of our services online.			https://www.zagbank.ca/	https://twitter.com/zagbank		\N	\N	Zag Bank has been a wholly owned subsidiary of Desjardins Group since being acquired in 2011.
47	Bench	Online bookkeeping for your business. We give you a team of bookkeepers to do your books, and simple, elegant software to track your finances.	1 (888) 760 1940	help@bench.co	https://bench.co/	https://twitter.com/bench		\N	\N	Bookkeeping is a universal point of pain for entrepreneurs, and we’re changing that.
48	Ferst Digital	Our purpose is to make you and your business more successful by bringing business banking into the 21st century. We`re building Canada`s first mobile-first API-based banking platform that helps startups and small businesses bank, manage their finances, and integrate all of their financial tools in a simple and intuitive way. 			https://www.ferstdigital.com/about	https://twitter.com/ferstdigital	https://www.linkedin.com/company-beta/11065927/	\N	\N	
49	Mentio	Automatic Cash Flow Forecast. Quickly see what money is coming in, and what bills you need to pay.  Catch any cash shortfalls before they happen. Get Paid Faster Mentio lets you know who to send reminders to – customers that usually pay late, and those that are already late. Plus, reminders are a breeze with one click emails.	(604) 352-2828	info@mentio.ca	http://www.mentio.ca/	https://twitter.com/mentiohq		\N	\N	
50	Wagepoint	By providing access to capital (Lending), improving cash flow (Payments), delivering money to employees (Payroll), helping businesses get paid (Invoicing), preparing for tax time and providing business insights (Accounting, Receipts), Wave covers the spectrum of a small business owner`s financial life, and helps businesses grow and thrive.						\N	\N	
51	Wave	online accounting, small business, software development, SaaS, invoicing and payments, cloud based accounting application, payroll, receipts, risk, and product development		info@waveapps.com	https://www.waveapps.com/	https://twitter.com/wavehq	https://www.linkedin.com/company-beta/1196866/	\N	\N	online accounting, small business, software development, SaaS, invoicing and payments, cloud based accounting application, payroll, receipts, risk, and product development
52	Carta Worldwide	Carta is an award-winning leader in digital transaction processing and enablement technologies, including Cloud Based Payments and Host Card Emulation (HCE), Tokenization, and Value Added Services such as Digital Offers, Loyalty and Stored Value solutions.	+1 416 840 5611	info@cartaworldwide.com	https://cartaworldwide.com/	https://twitter.com/cartaworldwide	https://www.linkedin.com/company-beta/574946/	\N	\N	Carta empowers banks, MNOs, OEMs, merchants, program managers and others to enable existing cardholder accounts or issue new and innovative products for the digital world. Carta’s unique Cloud Suite platform enables rapid deployment of mobile solutions with breakthrough technology that overcomes technical constraints of existing systems and infrastructure.
53	DreamPayments	Dream Payments enables merchants to sell everywhere using mobile devices. Dream’s cloud-based payment platform combined with its mobile point of sale device allows merchants to accept credit and debit cards, access rich analytics and reports, and provide digital receipts to customers. Dream’s solutions are secure, EMV compliant, and accept Contactless and Chip payment cards.			http://www.dreampayments.com/	https://twitter.com/dreampayments	https://www.linkedin.com/company-beta/5305951/	\N	\N	mobile point of sale, merchant solutions, credit card payments, debit card payments, merchant services, MPOS Platform, EMV, and payment processing
54	Ethoca	Ethoca is a secure network for card issuers and merchants to connect and work cooperatively outside the payment network in a unique and powerful way. Ethoca, through its innovative services, helps connect card issuers to online merchants to stop fraud that slips through their defences and recapture lost revenues.	203.6088039	sales@ethoca.com	https://www.ethoca.com/	https://twitter.com/Ethoca	https://www.linkedin.com/company-beta/113996/	\N	\N	thoca’s network consists of card issuing banks and online merchants working cooperatively together through a single, automated, secure connection outside the payment networks stream to their mutual benefit.
55	sounpays	Soundpays is a scalable, secure and ubiquitous mobile payment technology that enables transactions using inaudible sound waves from any speaker. We are the first and only technology to make purchasing directly from a video possible. Soundpays works using any device with a microphone and internet connection, allowing purchasing from TV & radio, online and in-store. 	1-800-341-1875		https://soundpays.com/		https://www.linkedin.com/company-beta/10135105/	\N	\N	
56	Tacit Innovations	Tacit Innovations is a fast growing technology company that is quickly being recognized as a relevant player transforming the way restaurants engage and service their customers. The company’s capabilities take advantage of mobile technologies and cutting edge platform development to enable innovative, progressive, relevant and easy to use customer experiences. Tacit Innovations is on a quest to revolutionize the way consumers discover, browse, order and pay for their meals at their favorite restaurant.	(866) 962-3426	info@tacitinnovations.com	http://tacitinnovations.com/		https://www.linkedin.com/company-beta/2929954/	\N	\N	maegan™ is the company’s initial offering targeted to serve the hospitality industry.
57	Braintree Payments	We provide the global commerce tools people need to build businesses, accept payments, and enable commerce for their users. It’s the simplest way to get paid for your great ideas -- across any device, and through almost any payment method. Merchants in more than 40 countries worldwide can accept, split, and enable payments in more than 130 currencies using Braintree.	877.511.5036	info@braintreepayments.com	braintreepayments.com	https://twitter.com/braintree		\N	\N	Acquired by PayPal 
60	Digital Retail Apps	Digital Retail Apps is a mobile application that enables users to scan, view, and purchase items through their smartphones.			http://digitalretailapps.com/about/	https://twitter.com/digitalretail		\N	\N	SelfPay extends retailers’ existing POS to the mobile device and offers a large selection of major payment methods -- all within a retailer branded in-app shopping experience. SelfPay is the natural and inevitable evolution of POS.
61	Berkeley Payment	Berkeley Payment Solutions delivers MasterCard and Visa prepaid card programs for corporations, governments, and financial institutions. As a trusted payment solutions provider, we help clients navigate the payments industry ecosystem and offer an extensive suite of services including prepaid card program management, product development, accounting and reporting, IT consulting and integration, processing, card design, and card fulfillment.	1-888-642-2881	info@berkeleypayment.com.	http://berkeleypayment.com/		https://www.linkedin.com/company/560764/	\N	\N	Prepaid Visa Card Programs, Rebate, Government, Reimbursements, Incentive, and Loyalty
62	CardSwap	The CardSwap platform offers Canadians a fast, easy, and completely secure way to sell unwanted gift cards for cash, buy gift cards and earn rewards, and even donate gift cards to a charitable cause.	1 (416) 491-2233	clientcare@cardswap.ca	https://www.cardswap.ca/			\N	\N	Online portal for buying, selling and Redeeming gift cards,
63	Dwello	 We`re proudly Canadian company that has been processing rental payments since 2012. We`ve been working together with Canadian payment providers, lawyers and jumping over hurdles to bring you this service to the property management landscape, and we`re only looking to do more.	     1 (877) 217-9185		http://www.dwello.com/	https://twitter.com/dwello		\N	\N	
64	Everpay	Everpay is an online, multinational payment transaction company that specializes in electronic payment solutions; providing a platform that allows for safe, secure payment transactions and money transfers to be made via the Internet. Online payment transactions have already gained popularity when shopping with credit cards and serve as an electronic alternative to paying with paper based methods, such as checks or money orders. With the advent of several technologies and Wi-Fi devices enabling the increased use of electronic payments Everpay has set very ambitious yet achievable targets in order to provide a new paradigm in the realm of financial service provision. 	+1 416 847 6786		http://www.everpayinc.com/	https://twitter.com/Everpay		\N	\N	
65	Flywire	Convenient, fast and secure, Flywire’s scalable platform provides currency conversion at exchange rates that can offer significant savings when compared to home-market banks and credit card providers. The company also supports its clients with end-to- end customer support including multilingual servicing via phone, email, and chat, as well as 24/7 online payment tracking. Company investors include Bain Capital Ventures, Spark Capital, FPrime Capital (formerly Devonshire), Accel Partners, and QED Investors	18003469252		https://www.flywire.com/	https://twitter.com/FlywireCo	https://www.linkedin.com/company-beta/434598/	\N	\N	global payments, tuition, financial services, universities, higher education, online, currency, payment processing, international students, international patients, and international receivables
66	Granify	Granify automatically maximizes revenue for online retailers by identifying shoppers that aren’t going to buy and changing their mind - before they leave the site - by harnessing the power of real-time big data and machine learning. We’re at the intersection of artificial intelligence and e-commerce, providing a SaaS solution that enables online retailers to maximise their sales by using cutting edge big data and machine learning technologies	1-844-(472-6439)		https://www.granify.com/	https://twitter.com/granify	https://www.linkedin.com/company-beta/2403940/	\N	\N	Big data analytics, Conversion, Online retailers, E-commerce, and Conversion rate optimization
67	Guusto	A new hospitality gifting app that offers a thoughtful and easy way to instantly treat clients, colleagues, family or friends to a little something when you can`t be there in person. Send drinks, food and more to wish happy birthday, thank, celebrate, reward, congratulate, impress, say sorry, encourage, cheer up, or just because.	778.331.1053	info@guusto.com	http://www.guusto.com/	https://twitter.com/guustogifts		\N	\N	iPhone App, Android App
68	Hubba	The purpose of Hubba is to allow brands and retailers to share “single source of truth” product information in real-time so consumers always have what they need to make the smartest and quickest purchase decisions. As time has gone on and Hubba has grown, we’re more than just sharing. Hubba now includes industry experts to connect with, a product discovery platform, industry news and an automated product listing platform that simplifies the process to work with big-box retailers like Walmart and Target.			http://www.hubba.com	https://twitter.com/hubba	https://www.linkedin.com/company-beta/2242581/	\N	\N	Product Content Management, Enterprise Software, Discovery Network, Digital Asset Management, Product Information Management, Product Information Network, and B2B Software
69	Hyperwallet	Hyperwallet’s global payout platform enables localized, multi-currency distribution to just about anywhere in the world. Our payout solutions are available as an SaaS or through REST API integration, and all include systems monitoring, maintenance management, payee support tools, and KYC/AML compliance. Our range of unique payout options ensure that your payees get their money quickly, conveniently, and affordably—wherever they are. 			https://www.hyperwallet.com/	https://twitter.com/hyperwallet	https://www.linkedin.com/company-beta/150981/	\N	\N	Online & Mobile Payments, International Payments, Financial Technology Solutions, Prepaid Cards, Global Bank Deposits, Virtual Prepaid Cards, Check Delivery, Direct to Card, and Cash Via Agent
70	LemonStand	LemonStand is a refreshingly customizable eCommerce platform for fast growing online retail brands. Easily design, create and publish content to grow organic website traffic, and educate customers using our flexible content management system.	 (604) 398-4188	sales@lemonstand.com	https://lemonstand.com/		https://www.linkedin.com/company-beta/438058/	\N	\N	Completely customize the entire user experience including the checkout process using your favorite front-end techniques. Partnered with Heap fro back-end data collection. eCommerce, Online retail, Cloud eCommerce, and web design Featured Groups
71	Lightspeed	Lightspeed is the leading provider of cloud-based point of sale solutions.  Lightspeed provides small and medium sized retail and restaurant businesses with point of sale solutions.	855-300-7108	info@lightspeedhq.com	https://www.lightspeedhq.com/	https://twitter.com/LightspeedHQ	https://www.linkedin.com/company-beta/1557218/	\N	\N	
72	Loadhub	A convenient and secure real-time financial solution that makes it easy for your customers to load money, make payments, or transfer funds at any of the 6,000 participating Canada Post locations across Canada. Loadhub uses Quick Response (QR) barcode technology integrated with Canada Post’s point-of-sale system. 	1-866-860-8838	loadhub@paymentsource.ca	https://www.paymentsource.ca/loadhub	https://twitter.com/LoadhubCA	https://www.linkedin.com/showcase/10461647/	\N	\N	
73	Lucova	We are disrupting the Point-of-Sale by re-imagining a solution that connects with our everyday smartphone devices. From Customer Recognition software, to Hands Free Payments, to Point of Experience software and everything in between, we’re bringing people, data and technology together to power everyday moments in-store.	1.866.338.7780	info@lucova.com	https://www.lucova.com/	https://twitter.com/lucova	https://www.linkedin.com/company-beta/3230683/	\N	\N	
74	Mobeewave	Mobeewave sees the evolution of cashless payment from cards towards ubiquitous mobile money. By equipping a mobile phone with secure, contactless payment-acceptance capability, Mobeewave’s award-winning patented technology powers a True Wallet Experience for consumers, allowing them to put cash in to their mobile wallets as well as make payments. With Mobeewave, anyone can accept cashless payment for anything, anyhow, enabling in-person consumer-to-consumer transactions, between strangers as well as friends and family, in a secure, trusted and open environment.	15145733287	info@mobeewave.com 	http://mobeewave.com/en/home-2/		https://www.linkedin.com/company-beta/2183970	\N	\N	
75	Nanopay	We are focused on 4 primary use cases for payments:\n1.  Cross-Border Payments Enable instantaneous, multi- currency transfer of funds across borders and continents; Satisfy diverse global risk and compliance requirements.\n2. B2B Payments Exchange goods and services with international trading partners with complete transparency and transaction context; Improve cash flow without exchanging sensitive banking information.\n3. Digital Cash Designed to enable person-to- person (P2P) payments online and in-store; Securely store and instantly transfer value with a collateralized, bearer-asset system.\n4.  Capital Markets Integrate digital cash into banks’ back-end systems to reduce clearing and settlement costs;  Free trapped capital caused by typical T+3 timeframes	+1 (416) 900-1111	info@nanopay.net	http://www.nanopay.net	https://twitter.com/nano_pay/	https://www.linkedin.com/company-beta/5183090/	\N	\N	Payments, Mobile Payments, Digital Cash, and Loyalty
76	nTrust	nTrust is a regulated and trusted platform that allows users to easily manage their bitcoin. Members can instantly purchase bitcoin from multiple currencies, store it, send it and receive it anywhere, or withdraw safely to a personal or recipient bank account. nTrust is working to provide equal opportunity and financial access to everyone, everywhere.	1-855-687-8788	help@nTrust.com	https://www.ntrust.com/	https://twitter.com/ntrust	https://www.linkedin.com/company-beta/2592620/	\N	\N	online money transfers, international money transfers, prepaid credit cards, international remittance, bitcoin, bitcoin exchange, and bitcoin wallets
77	Okanii	Okanii, we offer a secure, scalable and networked digital token (coin) based electronic cash payment platform solution for global commerce. Our solution provides a superior consumer experience, with an intuitive, simple, and easy to understand user interface, a high-strength cryptography-based security model, the convenience of mobile transactions, and unmatched consumer privacy. Our solution can be offered as a platform-as-a-service (PAAS) solution to financial institutions (FI’s), mobile carriers, merchant payment service providers (PSP’s), governments and central banks. It can support multiple transaction types, in any currency, for person-to-person (P2P), and consumer-to-business (C2B) payments and business-to-business (B2B) payments.	289.813.1121		https://www.okanii.com/			\N	\N	
78	Paycase	Paycase is a mobile-first universal remittance platform aimed at challenging and changing the traditional money transfer model. As a team, we have created a service that allows our customers to send money across the world for a fraction of the current industry standard; quickly, reliably, and on-the-go! 		support@paycase.com	https://www.paycase.com/	https://twitter.com/paycaseapp	https://www.linkedin.com/company-beta/9396458/	\N	\N	Online Money Transfers and Remittances
79	payfirma	Payfirma is an award-winning payments company that helps businesses accept credit and debit cards online, in-stores, and on mobile devices. Over 8,000 businesses across North America use Payfirma’s payment tools to get paid easily and keep all transaction data in one, simple place. When businesses use data to make decisions abut customers, products, and employees, they run smarter, more successful companies. 	+1 (800) 747-6883	info@payfirma.com	https://www.payfirma.com/	https://twitter.com/Payfirma	https://www.linkedin.com/company-beta/1662576/	\N	\N	
80	Payment Rails	Payment Rails is simplifying cross-border payouts for online marketplaces, share economy, crowdsourcing, affiliate platforms, app stores, and crowdfunding platforms. Make payouts to your independent contractors, affiliates and suppliers anywhere in the world in 150+ currencies through our payouts-as-a-service platform. We offer a powerful API or you can upload batch files through our dashboard portal. Recipients have the choice of how they want to receive their funds and in which currency: direct to their own bank account, credit card, prepaid card, cash pick-up, check, paypal + other options. 			https://www.paymentrails.com/	https://twitter.com/paymentrails	https://www.linkedin.com/company-beta/10001557/	\N	\N	
81	paymentus	 Our highly committed, creative employees turned an idea into a secure, SAAS-based Customer Engagement and Payment Platform; one that enables direct-bill organizations to provide a unified customer experience and boost adoption of cost-saving electronic billing and payment services.			https://www.paymentus.com/			\N	\N	
82	Paymobile	Paymobile Inc. provides carrier-class transaction solutions for consumers and corporate clients. It offers end-to-end payment solutions targeting various sales verticals, including government disbursements, payroll, corporate incentives, gift, consumer spend, social payments, and remittance. The company’s mobile technology allows customers to download a Paymobile app for their smartphone or use SMS messaging to perform functions, such as viewing their account balance and transaction history, and sending and receiving fund transfers in real-time. Its integrated money-transfer feature allows cardholders to transfer funds over their mobile device to another cardholder in real-time over the Visa Money Transfer platform. 	888-966-6246		www.paymobile.com	https://twitter.com/paymobileab	https://www.linkedin.com/company-beta/3830398/	\N	\N	Online rent and condo fees payment. and Resident portals
83	Payquad	Payquad Solutions was formed to simplify the rent payment process through digital automation. We have created an online platform which is amazingly simple to use. As partners of Visa, Mastercard, and American Express, we offer residents a variety of online payment methods including credit cards, debit cards and direct debit from your chequing account. In addition, our tenant portals come fully loaded with other great features like maintenance requests, a communication module and resident rewards to totally revolutionize the resident experience. 	1 888 385 9037	info@payquad.com	https://payquad.com	https://twitter.com/payquadinc	https://www.linkedin.com/company-beta/5391096/	\N	\N	
84	Pivotal Payments	Pivotal Payments is a leading provider of technology-driven global payment processing solutions to the point of sale, B2B and ecommerce industries. Our proprietary solutions include card not present, integrated POS and mobile payments that deliver efficiency and profitability to businesses across all sales channels and platforms. With a focus on security and fueled by continuous investment in research, product development and innovation, we are shaping the future of payments. Our goal is to maximize our clients`​ revenues and performance by empowering them with safer and smarter transactions.	1 866-693-2000		https://www.pivotalpayments.ca/	https://twitter.com/pivotalpayments	https://www.linkedin.com/company-beta/47266/	\N	\N	Merchant Accounts, Payment Processing, Global Acquiring, E-Commerce, Card Not Present Payments, Point-of-Sale (POS) Integration, Merchant Cash Advance, Multi-Currency Processing, Fraud Management, Gift Card & Loyalty, EMV Compliance, and Mobile Payments
85	Plastiq	Plastiq gives you the freedom to pay a bill or invoice with a credit card, even in situations you normally couldn`t. Now pay any bill with just a photo using the Plastiq mobile app. We`ll remind you when it`s due and let you pay in seconds with your favorite card. Time to say goodbye to stacks of bills with Plastiq! 			https://www.plastiq.com/	https://twitter.com/payplastiq	https://www.linkedin.com/company-beta/2642014	\N	\N	Payment Gateway, Payment Augmentation, and Processor Integrations
86	Plooto	Plooto is a payment management platform for business-to-business payments. We’re changing the world of commerce by making it easier, faster and cheaper for businesses to make payments.	(844)475-6686	info@plooto.co 	http://www.plooto.co		https://www.linkedin.com/company-beta/9460286/	\N	\N	Payments, B2B, Accounts Payable, Accounts Receivable, and Cloud Payment Processing
87	QicSEND	Qicsend is an online international money transfer service, offering safe, secure and affordable online remittance solutions. Our leading edge offerings are a fraction of the cost of traditional money transfers, making it easy and inexpensive to send money as often as you like to friends and family overseas.			https://www.qicsend.com/AboutUs	https://twitter.com/qicsend		\N	\N	QicSEND™ service is owned and operated by Toronto-based Mercury Mercantile Technologies, Inc. Comprising of a seasoned leadership team of industry veterans with significant international business expertise.
88	Remitbee	Remitbee is a worldwide money transfer and mobile money company specialized in online Money transfer. It is a part of Thamor Trading Corporation providing over 11 years of infrastructure and compliance expertise. Its a convenient and user friendly service in which customers can send money online using their web browsers, tablets and mobile phones. Remitbee`s goal is to end the ridiculous high cost of sending money overseas by cutting out the middleman or money transfer “agents” and sending money straight from customers to bank accounts and cash accounts worldwide. Remitbee is currently operational in Canada and will soon be available in the US and european countries to send money. They currently send money to 8 countries and more to come over the next few months.	 (905)290-8318	customercare@remitbee.com 	https://remitbee.com/		https://www.linkedin.com/company-beta/10100763/	\N	\N	Fintech, Mobile Money, Money Transfer, and Crowd Fundraise
89	Rentmoola	RentMoola, one of North America`s leading fintech companies is changing the landscape of paying rent all over the world. Solving the age-old problem that paying and collecting rent is a major hassle, RentMoola is an online global payment network that allows tenants and owners to pay rent and other payments by credit card, debit card, RM Direct Debit™ or RM Cash™ while earning rewards. Members have access to our MoolaPerks™ program that provides exclusive deals to travel, lifestyle, home services and other rewards redeemable across North America, UK and Europe.	 (888)665-8870	info@rentmoola.com	https://rentmoola.com/	https://twitter.com/rentmoola		\N	\N	
90	ReUp	ReUp is making mobile payment a reality for independent businesses. The platform helps local favourites, beloved mom-and-pops, and small shops capitalize on an industry worth over $235 billion globally as of 2013; by 2019, it is expected to be worth $142 billion in the US alone. ReUp allows users to build their own branded loyalty and payment app for iOS and Android devices. ReUp has built a simple drag and drop solution, resulting in the big brand experience without the big brand price tag. Completely self-service, ReUp has a global reach and is perfect for restaurants, cafes, salons and gyms looking to offer their customers a convenient mobile payment option.	1-855-984-0777	hello@getreup.com	http://www.getreup.com/	https://twitter.com/get_reup		\N	\N	
91	Shopify	Shopify is a leading cloud-based, multichannel commerce platform designed for small and medium-sized businesses. Merchants can use the software to design, set up and manage their stores across multiple sales channels, including web, mobile, social media. The platform also provides a merchant with a powerful back-office and a single view of their business. The Shopify platform was engineered for reliability and scale, using enterprise-level technology made available to businesses of all sizes. 			http://www.shopify.com	https://twitter.com/shopify	https://www.linkedin.com/company-beta/784652/	\N	\N	ecommerce, design, API, applications, being awesome, customer service, and ecommerce expertise
92	SmoothPay	SmoothPay is a free mobile app that allows you to use your phone to pay at participating locations and earn rewards. As you continue to use SmoothPay to make purchases, you increase your rewards progress. Once you reach your reward milestone, you earn credits towards your next purchase.			SmoothPay.com	https://twitter.com/smoothpayapp		\N	\N	
93	Square	We started with a little white card reader but haven’t stopped there. Our new reader helps our sellers accept chip cards and NFC payments, our Cash app lets people send money instantly and we’re building easy tools for customers, too. We’re empowering the electrician to send invoices, setting up the food truck with a delivery option, helping the clothing boutique pay its employees and giving the coffee chain capital for a second, third and fourth location.			https://squareup.com/ca/about	https://twitter.com/SquareCanada		\N	\N	
94	touchbistro	TouchBistro is an app that supports tableside ordering, custom restaurant layouts, custom menus, bill splitting, sales reports, and an unlimited number of order and cash register printers. It is available from the iTunes store. It is the top grossing Food and Beverage iTunes app in over 34 countries. TouchBistro does not require an Internet connection, communicating with printers/cash drawers via local WIFI.	416-363-5252	support@touchbistro.com	https://www.touchbistro.com/	https://twitter.com/touchbistro	https://www.linkedin.com/company-beta/2410023/	\N	\N	
111	DigaTrade	DIGATRADE is a global digital asset [Bitcoin] exchange, blockchain development services and distributed ledger technology company with located in Vancouver, Canada.	(604) 200-0071	info@digatrade.com	https://digatrade.com/	https://twitter.com/DigaTrade	https://www.linkedin.com/company-beta/10356074/	\N	\N	The proprietary DIGATRADE trading and matching engine manages high volume, high throughput, and low latency trading and was modelled on the same technology recently leveraged by the world’s largest investment banks.
95	VersaPay	VersaPay is a leading cloud-based invoice presentment and payment provider for businesses of all sizes. VersaPay`s ARC™ and PayPort™ software-as-a-service offerings allow businesses to easily deliver customized electronic invoices to their customers, to accept credit card and EFT/ACH payments and automatically reconcile payments to their ERP and accounting software. VersaPay is headquartered in Toronto, Canada and has operations in Montreal. Versapay provides businesses across Canada the ability to accept Visa, MasterCard, Discover, American Express, JCB and INTERAC Direct Debit at your place of business. Merchants can also increase your sales and retain customers by offering Gift & Loyalty Cards branded with your businesss logo and identity.	(866) 999-8729	info@versapay.com	http://www.versapay.com 		https://www.linkedin.com/company-beta/313431/	\N	\N	Payment Processing, Accounts Receivable Automation, Electronic Invoicing Payment & Presentment (EIPP), EFT, PCI, SaaS, Software as a Service, ACH, Accounts Receivable, accounts receivable software, cash application, and credit and collections management
96	vouchr	Vouchr drives transactions and adoption of payment systems with our unique engagement solution. Whitelabel SDK - An SDK that plugs into any app that offers payments and transforms the user experience. 		info@vouc.hr	https://vouc.hr/			\N	\N	
97	waypay	WayPay™ lets you access and combine any of your bank accounts or credit cards into a single source of funds to pay any business expense, in any currency, to anyone, in any way, from anywhere — even from your phone. 	 (844) 692-8581		https://www.waypay.ca/	https://twitter.com/WayPayInc		\N	\N	Innovative Payment Solutions, Strategic Payables Outsourcing, and Working Capital (Cash Management) Best Practices
98	Payment Evolution	PaymentEvolution provides easy to use, secure online payroll services to small and mid-sized businesses across Canada. Accountants, CPA firms and financial institutions rely on us for payroll expertise and services for their clientele. We’re bringing innovation and simplicity back into the payroll market – from new ways to pay your employees to our open developer program. 	 +1-647-776-7600	info@paymentevolution.com	http://www.paymentevolution.com	https://twitter.com/payevo	https://www.linkedin.com/company-beta/598996/	\N	\N	Payroll services, cloud based financial services, Payments, and mission critical business payments
99	Tilt	Tilt (formerly Crowdtilt) is a crowdfunding company that allows for groups and communities to collect, fundraise, or pool money online. The company is legally certified in securing fundraisers for non-profit organizations.			https://www.tilt.com/			\N	\N	We`re now part of the Airbnb family
100	UGO Mobile Solutions	UGO Wallet is a digital wallet app that allows you to get reward points, and to store and use loyalty, gift, membership, and other cards on your smartphone. You can also send money to friends and family. Plus, UGO Wallet lets you add, organize, and export your receipts.	 1-844-838-1192	 contactus@ugo.ca	https://www.ugo.ca/	https://twitter.com/ugo	https://www.linkedin.com/company-beta/9192624/	\N	\N	 Digital Payment, Loyalty Programs, and Digital Receipts
101	Vend	Sell in‑store, online & on‑the‑go with Vend. Vend is retail POS software, inventory management, ecommerce & customer loyalty for iPad, Mac and PC. Easily manage and grow your business in the cloud.	 1-844-814-5409		https://www.vendhq.com/		https://www.linkedin.com/company-beta/814954/	\N	\N	
102	BigBTC	BIGbtc Bitcoin Integration Group is the industry leader in Bitcoin Merchant Integration services. Focused on supporting the retail industry in Canada and USA			https://www.bigbtc.ca	https://twitter.com/BIGbtc	https://www.linkedin.com/company-beta/10298595/	\N	\N	BIGbtc brings clarity to the complexities associated with integrating Digital Currencies into the daily operations of storefront / online merchants, restaurateurs and many small business operators.
103	BitAccess	From start-ups to Fortune 500 companies, Bitaccess provides software services to power Fintech businesses in over 15 countries. Our customers rely on Bitaccess software and services to power their core infrastructure, compliance, security and support services. 			https://www.bitaccess.co/	https://twitter.com/bitaccess		\N	\N	Bitaccess blockchain based know-your-customer software enables near instant customer onboarding, while providing industry leading worldwide compliance coverage.
104	Blockchain Tech Ltd.	BTL offers enterprise class blockchain solutions to businesses with advanced trading and settlement needs, such as those in financial services ecosystem, energy and other commodity sectors. 	+1 855 256 5246	press@btl.co	http://btl.co/		https://www.linkedin.com/company-beta/10058900/	\N	\N	Having partnered with Visa, BTL built a prototype that showcases the extensive capabilities of a blockchain based interbank payment and settlement network built on BTL’s core platform, Interbit.
105	Bluzelle Networks	Bluzelle offers a complete and fully integrated stack of blockchain applications, middleware and data services. 		sales@bluzelle.com	http://bluzelle.com/	https://twitter.com/bluzellehq	https://www.linkedin.com/company-beta/10497135/	\N	\N	Payment Networks - Implement a real-time payment platform into your own ecosystem and open new channels. These can be applied to universities, mobile wallets, communities.
106	Bylls	Bylls is a simple, fast, and convenient service that allows Canadian individuals and businesses to pay their everyday bills using Bitcoin.			https://bylls.com/	https://twitter.com/myBylls		\N	\N	Bylls is 100% self-funded. No investors, no outside influence, completely independent.
107	Coinkite	Opendime is a small USB stick that allows you to spend Bitcoin like a dollar bill. Pass it along multiple times.		support@coinkite.com	https://coinkite.com/			\N	\N	Connect to any USB to check balance.
108	Coinpayments	Providing easy to use checkout systems and shopping cart integration for Bitcoin, Litecoin, Ethereum and other altcoins.			https://www.coinpayments.net/	https://twitter.com/CoinPaymentsNET		\N	\N	Offering plugins for all the popular webcarts used today. Register and install a plugin to set up CoinPayments with your new or pre-existing checkout.
109	Coinsquare	Coinsquare is the easiest platform to buy, sell and trade bitcoin, ethereum, gold and more. Made in Canada.		support@coinsquare.io	https://coinsquare.io/	https://twitter.com/coinsquare		\N	\N	We recognized the need for Canadian’s to have a safe, secure and “Canadian” destination to access the wonderful world of digital currency.
110	Cryptiv	Cryptiv`s Enterprise Blockchain Wallet System enables organizations to send, receive and store blockchain-enabled digital assets, while providing crucial administrative functions and ease of implementation. 		sales@cryptiv.com	https://cryptiv.com/	https://twitter.com/Cryptiv	https://www.linkedin.com/company-beta/5224074/	\N	\N	Cryptiv allows an organization to administer over teams of employees that have been provided limited access to a firm’s private keys through individual wallet accounts.
112	Goldmoney	Goldmoney Inc., a financial service company traded on the Toronto Stock Exchange (TSX:XAU), is a global leader in precious metal investment services and the world`s largest precious metals payment network.			https://www.goldmoney.com/	https://twitter.com/Goldmoney	https://www.linkedin.com/company-beta/1195274/	\N	\N	Safeguarding nearly $2 billion in assets for clients located in more than 150 countries, Goldmoney is focused on a singular mission to democratize access to gold and other precious metals through innovative technology.
113	Ledger Labs			info@ledgerlabs.com	https://ledgerlabs.com/	https://twitter.com/weareledger		\N	\N	
114	netcoins	Netcoins turns any computer, tablet, or mobile device into a virtual Bitcoin ATM.	(844)-515-COIN	info@goNetcoins.com	https://www.gonetcoins.com/	https://twitter.com/netcoinsca	https://www.linkedin.com/company-beta/3789778/	\N	\N	It`s the new way for customers to buy Bitcoin, and the easy way for stores to sell it.
115	Quadriga CX	Quadriga Fintech Solutions is a digital currency solutions provider, offering numerous services directed at the growing needs of the fintech community.	(604) 243-7056	contact@quadrigacx.com	https://www.quadrigacx.com/	https://twitter.com/QuadrigaCoinEx	https://www.linkedin.com/company-beta/3588418/	\N	\N	They currently offer Canada`s largest digital currency trading platform, merchant processing services, and international remittance services.
116	QuickBT	QuickBT.com is for buying Bitcoin quickly. When you buy Bitcoin from us, we do all the work and send it right to your address. Bitcoin can be sent to yourself, another person, or even directly to a merchant.	1-888-784-2555		https://quickbt.com/ca/?			\N	\N	
117	SecuraCoin	SecuraCoin is an Digital Currency Business	647-248-2646	info@securacoin.com	http://securacoin.com/		https://www.linkedin.com/company-beta/5194859/	\N	\N	SecuraCoin specialize in servicing Money Service Businesses (MSBs) and other retailers by creating technology and infrastructure for them to start using and providing digital currency services. 
118	Taurus Bitcoin Exchange	Taurus is Canada`s first fee-free bitcoin exchange that allows you to trade with other users at no cost.				https://twitter.com/taurusexchange	https://www.linkedin.com/company-beta/3824036/	\N	\N	Buying and selling happens in real time, 24/7! We constantly improve usability and develop new features based on our customers’ feedback.
119	vanbex	The Vanbex Group specializes in consulting, communications and development for blockchain businesses.	1 (888) 355-2976	hello@vanbex.com	https://www.vanbex.com/	https://twitter.com/vanbexgroup	https://www.linkedin.com/company-beta/3780798/	\N	\N	We take a tailored approach to shaping the future of every client we manage. We combine deep business insight with a strong understanding of how blockchain technology can impact existing business infrastructure and operations.
120	AdvisorStream	AdvisorStream is the only end-to-end solution for fully licensed client communications and first-to-market lead capture technology.	(416) 847-4701	info@advisorstream.com	http://www.advisorstream.com/	https://twitter.com/advisorstream	https://www.linkedin.com/company-beta/2884474/	\N	\N	First-to-market lead capture technology is built into every content piece – including personal and firm content.
121	Planswell	Planswell will help you build your wealth while saving a ton on taxes and fees.	1 (855) 752-6793	hello@planswell.com	https://planswell.com/	https://twitter.com/getplanswell	https://www.linkedin.com/company-beta/10785480/	\N	\N	Planswell puts some of the world’s largest and most secure asset managers, insurers and lenders in your plan. And our data security is fur real.
122	alvarnet	alvarPlan™ is a financial planning solution that helps financial advisors create financial plans for clients.	+1 888-291-5178	agile@alvarnet.com	https://www.alvarnet.com/			\N	\N	Build a full financial plan for a client in 20 minutes! alvarPlan™ provides the process, expertise and reassurance that you’re making the right recommendations for your clients.
123	Besurance Corporation	We are activating insightful research into actionable, data driven, real-life insurance products with customer engagement thru a community platform that is a unique, affordable and engaging.  	403-460-6961		http://besurance.ca/			\N	\N	Besure is a true p2p self-directed platform for empowerment and engagement.
124	ClientDesk	Canada`s leading digital platform for independent unsurance brokerages	1-800-481-8990	info@clientdesk.co	http://www.clientdesk.co/	https://twitter.com/clientdeskinc		\N	\N	Mobile and web platform that powers the core functions of the digital insurance experience including engagement, self-service and claims management.
125	Detego	Detego Technologies Inc is building the next generation of Financial Planning software.		info@detego.co	http://www.detego.co/		https://www.linkedin.com/company-beta/9292493/	\N	\N	Detego will give you small actionable steps to make your plan a reality.
126	EquityFeed	EquityFeed is the world`s #1 real-time platform helping individual stock traders stay more informed, intelligent, and profitable than ever. 		service@equityfeed	http://www.equityfeed.com/	https://twitter.com/EquityFeed	https://www.linkedin.com/company-beta/3065729/	\N	\N	EquityFeed`s real-time technology is the standard for active stock traders who need the absolute best intelligence tools to make informed, timely trade decisions. The platform is built in Java and available "on demand" from any computer with an internet connection.
127	hockeystick	Hockeystick is a financial market data network that provides deep insight into private companies.	1-800-593-6707 ext. 101	support@hockeystick.co	https://www.hockeystick.co/	https://twitter.com/hockeystickco/	https://www.linkedin.com/company-beta/4800508/	\N	\N	Hockeystick’s network connects a firm’s financial data directly to the venture capital, private equity and innovation ecosystems.
128	Idema Investments	Idema Investments offers individuals an innovative portfolio management solution that drastically reduces your total annual fees and helps you reach your retirement savings goal faster.	1-855-800-2550	info@idema.ca	https://www.idema.ca/en/	https://twitter.com/idema_ca	https://www.linkedin.com/company-beta/1080011/	\N	\N	
129	Invisor Investment Management	Invisor offers personalized goal-based investment solutions and online options to purchase life and health insurance	1.866.611.5431	hello@invisor.ca	https://invisor.ca/	https://twitter.com/invisorinvest	https://www.linkedin.com/company-beta/5318347/	\N	\N	With low fees and a convenient online platform that’s accessible 24/7, we’ll help you complete your financial plan and get the insurance coverage you need.
130	ModernAdvisor	ModernAdvisor is a start-up working on bringing online investment management to Canadians.	+1 888-365-0075	info@modernadvisor.ca	https://www.modernadvisor.ca/	https://twitter.com/ModernAdvsr	https://www.linkedin.com/company-beta/3571371/	\N	\N	The company complements expert investment insights with proprietary algorithms and an interactive website to bring sophisticated and low-cost investment management to every Canadian.
131	MotiveWave	MotiveWave Software is a well-established developer of easy-to-use full-featured charting, analysis and trading software built for the individual trader. 	416-840-4602	support@motivewave.com	http://www.motivewave.com/	https://twitter.com/MotiveWave		\N	\N	Their product "MotiveWave" has very advanced charting and drawing tools that are highly customizable and yet still easy-to-use, and also specializes in advanced analysis tools.
132	Mylo	First, Mylo automatically rounds up every purchase you make and invests the spare change. Then, once plugged in, let the magic happen.		support@mylo.ai	https://mylo.ai/	https://twitter.com/getmylo	https://www.linkedin.com/company-beta/10842490/	\N	\N	
133	Obsidian Solutions	Obsidian solutions is a cloud based software company specifically focused on helping fund managers grow their business by optimizing their investor relations efforts. 	1 800 990 4707	info@obsidiansi.com	https://www.obsidiansi.com/		https://www.linkedin.com/company-beta/4980038/	\N	\N	Our flagship product, Obsidian FundBinder - Helping Fund Managers Build Lasting Investor Relationships
134	Optimize Financial Group	Optimize is bringing Premiere Financial Services to individuals beyond the ultra rich and Efficient Corporate Finance Solutions to companies beyond the Fortune 500	1-866-209-6862	info@optimizefinancialgroup.com	https://www.optimizefinancialgroup.com/		https://www.linkedin.com/company-beta/9203386/	\N	\N	They are ensuring that every investor and every company gets open access to the right solutions for the right reasons. And it will take strategies tailored to each specific situation to accomplish this.
135	Questrade Wealth Management	At Questrade, we put you first and give you the respect you deserve. Because we are just like you: investors, savers, dreamers.	1.888.783.7866	fxcfd@questrade.com	http://www.questrade.com/why-questrade/home	https://twitter.com/questrade	https://www.linkedin.com/company-beta/94666/	\N	\N	They are transforming the Canadian financial landscape, one innovation at a time.
136	Responsive	Responsive offers bespoke hybrid and robo-advisory solutions to banks and private wealth managers.	778-869-5151		https://www.responsive.ai/	https://twitter.com/responsiveai	https://www.linkedin.com/company-beta/9477252/	\N	\N	Responsive is robo-advisor & technology company using Artificial Intelligence (AI) to make investment choices.
137	Smart Money Capital Management	Smart Money Capital Management ("Smart Money Invests") is one of Canada`s original online wealth managers (or more commonly referred to as a "Robo-Advisor"​). 	647-985-7626	info@smartmoneyinvest.ca	http://www.smartmoneyinvest.ca/	https://twitter.com/SMInvests	https://www.linkedin.com/company-beta/9462885/	\N	\N	Their  goal is to provide proven investment methodology to meet our client`s investment objectives
138	Veedata	Veedata is a platform that collects and analyzes health and lifestyle data from wearable devices and smartphones			http://veedata.io/	https://twitter.com/veedataio	https://www.linkedin.com/company-beta/6579501/	\N	\N	They offer predictive analytics and supports an insurer`s efforts in lowering their risk by engaging policyholders onto modifying their risk factors.
139	Voleo	Voleo is a social trading app that provides a new, more convenient, and engaging way to invest -together.			https://www.myvoleo.com/	https://twitter.com/MyVoleo	https://www.linkedin.com/company-beta/4857951/	\N	\N	
140	vuru	Stock analysis made simple.		support@vuru.co	http://www.vuru.co/	https://twitter.com/vurudotco		\N	\N	Vuru compiles massive amounts of financial, historical and real-time data into its analysis, giving you instant insight into the quality of a stock - without having to do all the work yourself
141	wealthica	Each day, Wealthica connects to your financial institutions and saves all your transaction history. It allows you to see all your investments in one place and get an unbiased view of your wealth.	1514.3123229		https://wealthica.com/	https://twitter.com/wealthica	https://www.linkedin.com/company-beta/10107948/	\N	\N	
142	Nest Wealth	Nest Wealth is Canada`s largest independent digital wealth management platform, offering both direct-to-investor solutions through www.nestwealth.com and advanced business-to-business solutions through our Nest Wealth Pro product	(647) 725-2559	questions@nestwealth.com	https://www.nestwealth.com/	https://twitter.com/nestwealth	https://www.linkedin.com/company-beta/9293204/	\N	\N	The only subscription based investing service in Canada, we charge our clients a low, flat monthly fee and custom-build every portfolio.
143	WealthBar Financial Services	Online investing with a human touch.	+1 888-373-7674	info@wealthbar.com	https://www.wealthbar.com/	https://twitter.com/wealthbar	https://www.linkedin.com/company-beta/2736754/	\N	\N	Our personal financial advisors make investing online effortless and help you build your brightest future possible!
144	Wealthsimple Financial	Wealthsimple is investing on autopilot. 	+1 855-255-9038		https://www.wealthsimple.com/en-ca/	https://twitter.com/Wealthsimple		\N	\N	Wealtsimple builds their customerrs a personal , low-cost profolio and puts their money to work like the world`s smartest investors
145	Secure Key	SecureKey is a leading identity and authentication provider that simplifies consumer access to online services and applications.	(416) 477-5625	info@securekey.com	securekey.com/	https://twitter.com/securekey	https://www.linkedin.com/company-beta/481905/	\N	\N	SecureKey’s next generation privacy-enhancing services enable consumers to conveniently and privately assert identity information using trusted providers, such as banks, telcos and governments, helping them connect to critical online services with a digital credential they already have and trust.
146	camouflage	Imperva acquired Camouflage in 2016 and is now offering data masking solutions under the Imperva Camouflage banner.	(709) 722-1200	info@datamasking.com	https://datamasking.com/	https://twitter.com/CamouflageDM	https://www.linkedin.com/company-beta/256975/	\N	\N	The company’s innovative, customized, and experience-driven approach has helped many fortune 500 companies protect their sensitive data, and corporate reputations.
147	Securefact	Securefact is a trusted provider of advisory services, on-demand and managed services for our customers`​ know-your-customer (KYC) & customer identification needs, regulatory compliance requirements and secured lending portfolios.Securefact 	(416) 979-5858	info@securefact.com	www.securefact.com/	https://twitter.com/securefact	https://www.linkedin.com/company-beta/1119891/	\N	\N	Leveraging our proprietary technology, Advanced workflow and subject matter expertise, we convert time consuming manual and otherwise sub-optimal processes into efficient, effective and fully compliant solutions.
163	R2Crowd	R2 is Canada’s only national online marketplace for real estate investing and raising capital.	647 292 4769	info@r2-re.com	https://www.r2-re.com/	https://twitter.com/r2investments	https://www.linkedin.com/company-beta/10293534/	\N	\N	The platform offers solutions across the entire capital stack including equity (JV, Preferred, and Syndicated Mortgage), and debt (1st & 2nd secured).
148	nymi	Nymi’s vision is to deliver Always On Authentication™, transforming the model of authentication into a secure and seamless experience for both enterprises and end-users.	(416) 977-3042	info@nymi.com	https://nymi.com/	https://twitter.com/nymiband	https://www.linkedin.com/company-beta/3241521/	\N	\N	Nymi’s first product, the Nymi Band, is a wearable device that delivers biometrically secured, persistent authentication. The Nymi Band utilizes strong biometric modalities such as HeartID™ – a proprietary technology that leverages the wearer’s unique cardiac signature (or ECG) as a biometric identifier.
149	Security Compass	Security Compass is a software security company that provides professional services, training, and a first-of-its kind Software Security Requirements Management (SSRM) platform to help eliminate security vulnerabilities in mission-critical applications, minimize organizational risk, and easily meet regulatory and compliance standards.	+1 888-777-2211	info@securitycompass.com	https://www.securitycompass.com/	https://twitter.com/securitycompass		\N	\N	We guide your team in building a customized security blueprint based on your SDLC and business needs to cost-effectively mitigate risks.
150	Trulioo	Trulioo offers the most robust and comprehensive global identity verification solution in the market.	1 (888) 773-0179	 support@trulioo.com	https://www.trulioo.com/	https://twitter.com/trulioo	https://www.linkedin.com/company-beta/1491350/	\N	\N	Through one single portal/API, Trulioo can assist you with all your AML/KYC identity verification requirements by providing secure access to over 4 billion identities worldwide.
151	verafin	Verafin is a leader in cloud-based, cross-institutional Fraud Detection and Anti-Money Laundering (FRAMLx) collaboration software with a customer base of over 1500 financial institutions across North America. 	+1 877-368-9986	info@verafin.com	https://verafin.com/	https://twitter.com/verafin	https://www.linkedin.com/company-beta/274884/	\N	\N	Its solution uses advanced cross-institutional, behavior-based analytics to help financial institutions stay a step ahead of numerous types of fraud as well as the BSA, USA PATRIOT Act, and FACTA compliance landscape, while allowing them to collaborate cross-institutionally.
152	esentire	eSentire® is the largest pure-play Managed Detection and Response (MDR) service provider, keeping organizations safe from constantly evolving cyber-attacks that technology alone cannot prevent. 	(519) 651-2200	info@esentire.com	https://www.esentire.com/	https://twitter.com/eSentire	https://www.linkedin.com/company-beta/150760/	\N	\N	Its 24x7 Security Operations Center (SOC), staffed by elite security analysts, hunts, investigates, and responds in real-time to known and unknown threats before they become business disrupting events.
153	Aspire	The Aspire Gateway platform is a software infrastructure solution enabling better loan data, reporting and analytics flow between Loan Originators, Investors and Banks.			http://aspirefintech.com/	https://twitter.com/AspireFinTech1	https://www.linkedin.com/company/10538817/	\N	\N	Aspire enables Originators, Investors, and Banks to better access, unlock and empower lending data.
154	Copower	Copower helps investors place capital into energy efficiency and renewable energy generation projects that offer solid returns, along with measured environmental impact	1-833-267-6937		https://copower.me/en/	https://twitter.com/CoPowerInc	https://www.linkedin.com/company/copowerinc	\N	\N	CoPower is an online platform that simplifies clean energy investing.
155	Crowdmatix	Crowdmatrix aims to change the investment landscape by leveraging technology to make diversifying into alternative asset classes simple and efficient.			http://crowdmatrix.co/home/	https://twitter.com/crowdmatrixinc	https://www.linkedin.com/company/10287787	\N	\N	Crowdmatrix is an online investment marketplace that connects accredited investors with vetted alternative investment opportunities.
156	Fincad	FINCAD is the leading provider of sophisticated valuation and risk analytics for multi-asset derivative and fixed income portfolios.	+1.604.957.1200	info@fincad.com	http://www.fincad.com/	https://twitter.com/fincad	https://www.linkedin.com/company-beta/32172/	\N	\N	FINCAD helps over 1,000 global financial institutions enhance returns, manage risk, reduce costs, comply with regulations, and provide confidence to investors and shareholders.
157	frontfundr	FrontFundr is a registered financial services firm that combines advanced compliance technology and digital media, under existing investment legislation, to give both new and seasoned investors ready access to stringently screened, market ready businesses—prepared and guided from business pitch to deal completion.	+1 800-804-1524		https://www.frontfundr.com/	https://twitter.com/frontfundr	https://www.linkedin.com/company-beta/3707631/	\N	\N	They present thier investor community with only the highest quality deals that have undergone our stringent in-house due diligence and advanced online compliance process, set to standards beyond Canadian regulatory requirements.
158	MarketIQ	The first big data analytics platform, built from the ground up specifically for financial institutions, to augment the decision making process. 	1-800-604-0647	info@themarketiq.com	http://themarketiq.com/	https://twitter.com/themarketiq	https://www.linkedin.com/company/market-iq-inc	\N	\N	Market IQ`s Predictive Analytics Platform helps financial institutions make smarter, more informed data-driven business decisions.
159	Nexuscrowd	NexusCrowd Inc. is a registered Exempt Market Dealer in the Provinces of Ontario, Alberta and British Columbia.	1.877.487.6506      	info@nexuscrowd.com	http://www.nexuscrowd.com/	http://twitter.com/nexuscrowd	https://www.linkedin.com/company-beta/5338518/	\N	\N	NexusCrowd unlocks access to some of Canada’s best private real estate investment opportunities that were previously limited to institutions and individuals
160	Overbond	Overbond is enhancing primary market activity with digital tools and processes that eliminate inefficiency while increasing transparency and reducing costs to allow stakeholders to focus on high value activity.	+1 844-343-2663	info@overbond.com	https://www.overbond.com/	https://twitter.com/overbond	https://www.linkedin.com/company/overbond	\N	\N	Overbond is transforming how global investment banks, institutional investors, corporations and governments connect and access the primary fixed income market.
161	Q4	Q4 is a global leader in cloud-based investor relations and capital market solutions. Our mission is to deliver innovative, dynamic and secure websites, webcasting and intelligence solutions that equip our clients with the information and tools they need to make more effective decisions, better engage with shareholders, and understand the capital markets.	(416) 626-7829	support@q4inc.com	https://www.q4inc.com/	http://twitter.com/q4tweets	https://www.linkedin.com/company-beta/206721/	\N	\N	Q4 Cloud offers the most flexible and feature-rich investor relations and corporate websites in the market today.
162	quandl	Quandl’s platform is used by over 200,000 people including analysts from the world’s top hedge funds, asset managers, and investment banks.			https://www.quandl.com/	https://twitter.com/quandl	https://www.linkedin.com/company-beta/2459204	\N	\N	The premier source for financial, economic, and alternative datasets, serving investment professionals.
164	Street Contxt	Street Contxt is a global knowledge exchange for institutional finance.	(416) 596-9011	sales@streetcontxt.com	https://streetcontxt.com/	https://twitter.com/streetcontxt	https://www.linkedin.com/company-beta/2971239/	\N	\N	The platform provides smart, actionable insights; helping to create a more efficient Capital Markets system.
165	Ticksmith	TickSmith, with its TickVault platform based on hadoop technology, is a leader in Big Data applications for the Brokerage ecosystem and financial services.	+1 514 360 6369		https://www.ticksmith.com/	https://twitter.com/TickSmith	https://www.linkedin.com/company-beta/2906183/	\N	\N	The platform is used for data centralization and distribution, market surveillance, risk management, strategy discovery and analytics.
166	Seedlify	Canada`s first revenue based financing platform that makes it easy for investors to invest in early-growth tech companies.		hello@seedlify.com	https://seedlify.com/	https://twitter.com/seedlify	https://www.linkedin.com/company-beta/10685310/	\N	\N	Seedlify is making it easy for Canadian tech companies to access fast, fairly priced, and user friendly growth capital while providing investors with greater peace of mind.
\.


--
-- Data for Name: company_businessmodel; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY company_businessmodel (business_model_id, company_id) FROM stdin;
2	1
1	2
1	3
2	3
2	4
1	5
1	6
2	7
2	8
2	9
1	10
2	10
1	11
2	12
1	13
1	14
2	15
1	16
1	17
2	17
2	18
2	19
1	20
2	21
2	22
2	23
2	24
2	25
2	26
1	27
1	28
1	29
1	30
1	31
1	32
1	33
2	34
1	35
1	36
2	37
2	38
2	39
2	40
1	41
2	42
2	43
2	44
1	45
1	46
1	47
1	48
1	49
1	50
1	51
1	52
1	53
1	54
2	55
1	56
1	57
1	58
2	58
1	59
2	60
1	61
2	62
1	63
1	64
1	65
1	66
2	67
1	68
1	69
1	70
1	71
1	72
1	73
2	74
1	75
2	75
2	76
2	77
2	78
1	79
2	80
1	81
1	82
2	83
1	84
1	85
2	85
1	86
2	87
2	88
2	89
1	90
1	91
1	92
1	93
1	94
1	95
1	96
1	97
1	98
2	99
2	100
1	101
2	102
2	103
2	104
2	105
2	106
2	107
2	108
2	109
2	110
2	111
2	112
2	113
2	114
2	115
2	116
1	117
2	118
2	119
1	120
2	120
2	121
2	122
2	123
1	124
2	125
2	126
2	127
2	128
2	129
2	130
2	131
2	132
2	133
2	134
2	135
1	136
2	136
2	137
2	138
2	139
2	140
2	141
2	142
2	143
2	144
2	145
1	145
1	146
2	147
2	148
2	149
1	149
2	150
2	151
2	152
1	153
2	154
2	155
2	156
2	157
2	158
2	159
1	159
2	160
1	160
2	161
1	161
2	162
1	162
2	163
2	164
1	164
2	165
1	165
2	166
1	166
\.


--
-- Data for Name: company_capabilities; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY company_capabilities (capability_id, company_id) FROM stdin;
3	1
1	2
1	3
1	4
2	5
2	6
2	7
1	8
2	9
1	10
1	11
2	12
3	13
3	13
1	13
1	14
1	15
1	16
1	17
1	18
2	19
1	20
1	21
1	22
1	23
2	24
1	25
2	26
1	27
1	28
1	29
3	30
1	31
1	32
1	33
1	34
1	35
1	36
1	37
1	38
1	39
1	40
1	41
2	42
2	43
1	44
1	45
1	46
1	47
1	48
1	49
1	50
1	51
1	52
1	53
1	54
1	55
1	56
1	57
1	58
1	59
1	60
1	61
1	62
1	63
1	64
1	65
1	66
1	67
1	68
1	69
1	70
1	71
1	72
1	73
1	74
1	75
1	76
1	77
1	78
1	79
1	80
1	81
1	82
1	83
1	84
1	85
1	86
1	87
1	88
1	89
1	90
1	91
1	92
1	93
1	94
1	95
1	96
1	97
1	98
1	99
1	100
1	101
1	102
1	103
3	104
1	104
1	105
1	106
1	107
1	108
1	109
1	110
1	111
1	112
1	113
1	114
1	115
1	116
1	117
1	118
1	119
1	120
1	121
1	122
2	123
1	124
1	125
2	126
1	127
2	128
2	129
1	130
1	131
1	132
1	133
1	134
1	135
1	136
1	137
1	138
1	139
1	140
1	141
1	142
1	143
1	144
1	145
1	146
1	147
1	148
1	149
1	150
1	151
1	152
1	153
1	154
1	155
1	156
1	157
1	158
2	159
1	160
1	161
1	162
1	163
1	164
1	165
1	166
\.


--
-- Data for Name: company_category; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY company_category (category_id, company_id) FROM stdin;
1	1
1	2
1	3
1	4
1	5
1	6
1	7
1	8
3	8
1	9
1	10
1	11
1	12
1	13
1	14
2	14
3	14
4	14
1	15
1	16
1	17
1	18
3	18
1	19
1	20
1	21
1	22
1	23
1	24
1	25
3	25
1	26
1	27
1	28
2	28
5	28
1	29
1	30
1	31
1	32
3	32
1	33
7	33
2	34
2	35
2	36
9	36
2	37
3	37
2	38
9	38
2	39
1	40
2	40
2	41
2	42
5	42
2	43
1	44
2	44
2	45
1	46
2	46
2	47
2	48
2	49
1	50
2	50
3	50
10	51
1	52
3	52
3	53
3	54
7	54
3	55
3	56
2	57
3	57
3	58
3	59
3	60
3	61
3	62
3	63
3	64
3	65
3	66
3	67
3	68
3	69
3	70
3	71
3	72
3	73
3	74
3	75
8	75
2	76
3	76
4	76
8	76
3	77
4	77
3	78
3	79
3	80
3	81
3	82
3	83
3	84
3	85
3	86
3	87
3	88
3	89
3	90
3	91
3	92
3	93
3	94
3	95
3	96
3	97
3	98
3	99
3	100
3	101
4	102
4	103
4	104
4	105
4	106
4	107
4	108
4	109
4	110
4	111
4	112
4	113
4	114
4	115
4	116
4	117
4	118
4	119
5	120
5	121
6	121
5	122
5	123
5	124
5	126
1	127
5	127
5	128
2	129
5	129
5	130
5	131
5	132
2	133
5	133
5	134
5	135
6	136
5	137
5	138
5	139
5	140
5	141
1	142
5	142
6	142
2	143
6	143
1	144
5	144
6	144
2	145
7	146
7	147
7	148
7	149
7	150
7	151
7	152
1	153
8	154
8	155
8	156
5	157
8	158
1	159
5	159
8	159
1	160
5	160
8	160
1	161
5	161
8	161
1	162
2	162
5	162
2	163
5	163
8	163
2	164
8	164
2	165
5	165
8	165
2	166
8	166
\.


--
-- Data for Name: company_group; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY company_group (group_id, company_id) FROM stdin;
2	1
2	2
2	3
2	4
2	5
2	6
2	7
2	8
2	9
2	10
2	11
2	12
2	13
2	14
2	15
2	16
2	17
2	18
2	19
2	20
2	21
2	22
2	23
2	24
1	25
1	26
1	27
1	28
1	29
1	30
1	31
1	32
1	33
1	34
1	35
1	36
1	37
1	38
1	39
1	40
1	41
1	42
1	43
1	44
1	45
1	46
1	47
1	48
1	49
1	50
1	51
1	52
1	53
1	54
1	55
1	56
1	57
1	58
1	59
1	60
6	61
3	62
3	63
3	64
3	65
3	66
3	67
3	68
3	69
3	70
3	71
3	72
3	73
3	74
3	75
3	76
3	77
3	78
3	79
3	80
3	81
3	82
3	83
3	84
3	85
3	86
3	87
3	88
3	89
3	90
3	91
3	92
3	93
3	94
3	95
3	96
3	97
3	98
3	99
3	100
3	101
3	102
3	103
3	104
3	105
3	106
3	107
3	108
3	109
3	110
3	111
3	112
3	113
3	114
3	115
3	116
3	117
3	118
3	119
3	120
3	121
3	122
3	123
3	124
3	125
3	126
3	127
3	128
3	129
3	130
3	131
3	132
3	133
3	134
3	135
3	136
3	137
3	138
3	139
3	140
3	141
3	142
3	143
3	144
3	145
3	146
3	147
3	148
5	148
3	149
5	149
5	150
5	151
5	152
5	153
5	154
5	155
5	156
5	157
5	158
5	159
5	160
5	161
5	162
5	163
5	164
1	165
5	166
\.


--
-- Name: company_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('company_id_seq', 1, false);


--
-- Data for Name: company_location; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY company_location (location_id, company_id) FROM stdin;
4	1
22	2
22	3
4	4
17	5
12	6
22	7
22	8
4	9
22	10
22	11
22	12
4	13
22	14
22	15
19	16
22	17
16	18
4	19
22	20
22	21
3	22
4	23
4	24
22	24
4	25
14	26
4	27
22	28
22	29
22	30
18	31
3	32
22	33
22	34
22	35
4	36
4	37
19	38
22	39
22	40
7	41
22	42
22	43
3	44
22	45
22	46
4	47
19	48
4	49
15	50
22	51
22	52
22	53
22	54
22	55
22	56
2	57
21	58
4	59
22	60
22	61
22	62
22	63
19	64
9	65
22	65
20	66
4	67
22	68
4	69
4	70
19	71
22	72
22	73
19	74
22	75
4	76
22	77
22	78
4	79
22	80
13	81
22	82
22	83
19	84
1	85
22	86
22	87
22	88
4	89
22	90
21	91
22	92
1	93
22	94
22	95
22	96
5	97
22	98
1	99
22	100
22	101
22	102
22	103
4	104
4	105
19	106
22	107
4	108
22	109
22	110
4	111
22	112
22	113
4	114
4	115
22	116
22	117
4	118
4	119
22	120
22	121
15	122
3	123
22	124
19	125
19	126
22	127
19	128
11	129
4	130
8	131
19	132
22	133
22	134
22	135
4	136
22	137
22	138
15	138
4	139
22	140
19	141
22	142
4	143
22	144
22	145
6	146
22	147
22	148
22	149
4	150
6	151
10	152
22	153
19	154
22	155
4	156
4	157
22	158
22	159
22	160
22	161
22	162
22	163
22	164
19	165
19	166
\.


--
-- Data for Name: company_partner; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY company_partner (partner_id, company_id) FROM stdin;
1	1
2	13
3	13
4	30
5	104
\.


--
-- Data for Name: groupco; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY groupco (id, name, code) FROM stdin;
1	Financial Technology	FIN_TECH
2	Financial Services	FIN_SERV
3	Financial Technology Ventures	FIN_VENT
4	Support	SUPP
5	Service Providers	SERV_PROV
6	Financial Institutions	FIN_INST
\.


--
-- Name: groupco_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('groupco_id_seq', 1, false);


--
-- Data for Name: location; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY location (id, name, geo_location) FROM stdin;
1	San Francisco - California	37.77493,-122.419416
2	Chicago - Illinois	41.878114,-87.629798
3	Calgary - Alberta	51.0486151,-114.071169
4	Vancouver - British Columbia	49.2827291,-123.120738
5	Burlington - Ontario	43.32552,-79.799032
6	St. John - Newfoundland and Labrador	47.5615096,-52.712577
7	Mountain View - California	37.3860517,-122.0838511
8	Kelowna - British Columbia	49.8879173,-119.495901
9	Boston - Massachusetts	42.3600825,-71.05888
10	Cambridge - Ontario	43.3616211,-80.314428
11	Oakville - Ontario	43.467517,-79.687666
12	Burnaby - British Columbia	49.2488091,-122.98051
13	Richmond Hill - Ontario	43.88284,-79.440281
14	New York - New York	40.7127837,-74.005941
15	Waterloo - Ontario	43.4642578,-80.52041
16	London - Ontario	42.9849233,-81.245277
17	Victoria - British Columbia	48.4284207,-123.365644
18	London - UK	51.507351,-0.127758
19	Montreal - Quebec	45.5016889,-73.567256
20	Edmonton - Alberta	53.544389,-113.490927
21	Ottawa - Quebec	45.4373486,-75.63875
22	Toronto - Ontario	43.653226,-79.383184
\.


--
-- Name: location_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('location_id_seq', 1, false);


--
-- Data for Name: partner; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY partner (id, name) FROM stdin;
1	SEI
2	CREDIC UNIONS
3	BANKS
4	LENDIFIED
5	VISA
\.


--
-- Name: partner_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('partner_id_seq', 1, false);


--
-- Data for Name: user; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY "user" (id, username, email, password_hash) FROM stdin;
\.


--
-- Name: user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('user_id_seq', 1, false);


--
-- Name: alembic_version alembic_version_pkc; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY alembic_version
    ADD CONSTRAINT alembic_version_pkc PRIMARY KEY (version_num);


--
-- Name: business_model business_model_code_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY business_model
    ADD CONSTRAINT business_model_code_key UNIQUE (code);


--
-- Name: business_model business_model_name_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY business_model
    ADD CONSTRAINT business_model_name_key UNIQUE (name);


--
-- Name: business_model business_model_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY business_model
    ADD CONSTRAINT business_model_pkey PRIMARY KEY (id);


--
-- Name: capability capability_code_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY capability
    ADD CONSTRAINT capability_code_key UNIQUE (code);


--
-- Name: capability capability_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY capability
    ADD CONSTRAINT capability_pkey PRIMARY KEY (id);


--
-- Name: category category_code_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY category
    ADD CONSTRAINT category_code_key UNIQUE (code);


--
-- Name: category category_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY category
    ADD CONSTRAINT category_pkey PRIMARY KEY (id);


--
-- Name: company company_name_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY company
    ADD CONSTRAINT company_name_key UNIQUE (name);


--
-- Name: company company_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY company
    ADD CONSTRAINT company_pkey PRIMARY KEY (id);


--
-- Name: groupco groupco_code_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY groupco
    ADD CONSTRAINT groupco_code_key UNIQUE (code);


--
-- Name: groupco groupco_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY groupco
    ADD CONSTRAINT groupco_pkey PRIMARY KEY (id);


--
-- Name: location location_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY location
    ADD CONSTRAINT location_pkey PRIMARY KEY (id);


--
-- Name: partner partner_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY partner
    ADD CONSTRAINT partner_pkey PRIMARY KEY (id);


--
-- Name: user user_email_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "user"
    ADD CONSTRAINT user_email_key UNIQUE (email);


--
-- Name: user user_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "user"
    ADD CONSTRAINT user_pkey PRIMARY KEY (id);


--
-- Name: user user_username_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "user"
    ADD CONSTRAINT user_username_key UNIQUE (username);


--
-- Name: ix_capability_name; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX ix_capability_name ON capability USING btree (name);


--
-- Name: ix_category_name; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX ix_category_name ON category USING btree (name);


--
-- Name: ix_groupco_name; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX ix_groupco_name ON groupco USING btree (name);


--
-- Name: ix_location_name; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX ix_location_name ON location USING btree (name);


--
-- Name: ix_partner_name; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX ix_partner_name ON partner USING btree (name);


--
-- Name: company_businessmodel company_businessmodel_business_model_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY company_businessmodel
    ADD CONSTRAINT company_businessmodel_business_model_id_fkey FOREIGN KEY (business_model_id) REFERENCES business_model(id);


--
-- Name: company_businessmodel company_businessmodel_company_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY company_businessmodel
    ADD CONSTRAINT company_businessmodel_company_id_fkey FOREIGN KEY (company_id) REFERENCES company(id);


--
-- Name: company_capabilities company_capabilities_capability_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY company_capabilities
    ADD CONSTRAINT company_capabilities_capability_id_fkey FOREIGN KEY (capability_id) REFERENCES capability(id);


--
-- Name: company_capabilities company_capabilities_company_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY company_capabilities
    ADD CONSTRAINT company_capabilities_company_id_fkey FOREIGN KEY (company_id) REFERENCES company(id);


--
-- Name: company_category company_category_category_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY company_category
    ADD CONSTRAINT company_category_category_id_fkey FOREIGN KEY (category_id) REFERENCES category(id);


--
-- Name: company_category company_category_company_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY company_category
    ADD CONSTRAINT company_category_company_id_fkey FOREIGN KEY (company_id) REFERENCES company(id);


--
-- Name: company_group company_group_company_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY company_group
    ADD CONSTRAINT company_group_company_id_fkey FOREIGN KEY (company_id) REFERENCES company(id);


--
-- Name: company_group company_group_group_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY company_group
    ADD CONSTRAINT company_group_group_id_fkey FOREIGN KEY (group_id) REFERENCES groupco(id);


--
-- Name: company_location company_location_company_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY company_location
    ADD CONSTRAINT company_location_company_id_fkey FOREIGN KEY (company_id) REFERENCES company(id);


--
-- Name: company_location company_location_location_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY company_location
    ADD CONSTRAINT company_location_location_id_fkey FOREIGN KEY (location_id) REFERENCES location(id);


--
-- Name: company company_owner_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY company
    ADD CONSTRAINT company_owner_id_fkey FOREIGN KEY (owner_id) REFERENCES "user"(id);


--
-- Name: company_partner company_partner_company_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY company_partner
    ADD CONSTRAINT company_partner_company_id_fkey FOREIGN KEY (company_id) REFERENCES company(id);


--
-- Name: company_partner company_partner_partner_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY company_partner
    ADD CONSTRAINT company_partner_partner_id_fkey FOREIGN KEY (partner_id) REFERENCES partner(id);


--
-- PostgreSQL database dump complete
--

