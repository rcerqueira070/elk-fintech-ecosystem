
curl -XPUT "http://localhost:9200/ecosystem" -H 'Content-Type: application/json' -d'
{
  "settings": {
    "analysis": {
      "analyzer": {
        "std_english": { 
          "type": "standard",
          "stopwords_path": "stopwords/stop_words.txt"
        }
      }
    }
  },
  "mappings" : {
    "company":{
        "properties": {
            "description": { 
            "type":     "text",
            "fielddata": true,
            "analyzer": "std_english", 
            "fields": {
            "english": {
                "type":     "text",
                "analyzer": "std_english" 
                }
                }
            },
            "key_features": { 
            "type":     "text",
            "fielddata": true,
            "analyzer": "std_english", 
            "fields": {
            "english": {
                "type":     "text",
                "analyzer": "std_english" 
                }
                }
            },
            "geo_loc": {
                "type": "geo_point"
            }
        }
    }
  }  
}'
